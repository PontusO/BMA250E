Bosch BMA250 class for Arduino
========

You can use this class to allow your Arduino based system to communicate with a Bosch BMA250 Accelerometer through I2C.

##Main features
* Easy access to all accelerations (X, Y and X);
* Ready to use magnitude acceleration (R); 
